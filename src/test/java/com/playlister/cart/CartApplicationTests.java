package com.playlister.cart;

import com.playlister.cart.model.CartList;
import com.playlister.cart.model.Song;
import com.playlister.cart.service.CartService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class PracApplicationTests {

    private final String URL = "/api/cart";
    @Autowired
    TestRestTemplate restTemplate;

    @Autowired
    CartService cartService;

    CartList cartList;

    @BeforeEach
    void setup() {
        List<Song> testTracks = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            Song s = new Song((long) i, "title" + i, "artist" + i, "album" + i);
            testTracks.add(s);
        }
        cartList = new CartList(testTracks);
        cartService.setCartList(cartList);
    }

    @Test
    void contextLoads() {
    }

    @Test
    void getTracks_None_ReturnsTrackList() {
        ResponseEntity<CartList> response = restTemplate.getForEntity(URL, CartList.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(5, response.getBody().getCart().size());
    }

    @Test
    void getTracks_None_NoContent() {
        cartService.setCartList(new CartList());
        ResponseEntity<CartList> response = restTemplate.getForEntity(URL, CartList.class);

        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }

    @Test
    void deleteSong_SongId_Success() {
        ResponseEntity<CartList> cart = restTemplate.getForEntity(URL, CartList.class);

        assertEquals(HttpStatus.OK, cart.getStatusCode());
        assertNotNull(cart.getBody());
        assertTrue(hasSongId(1, cart.getBody()));

        restTemplate.delete(URL + "/1");

        ResponseEntity<CartList> cart2 = restTemplate.getForEntity(URL, CartList.class);

        assertEquals(HttpStatus.OK, cart2.getStatusCode());
        assertNotNull(cart2.getBody());
        assertFalse(hasSongId(1, cart2.getBody()));
    }

    @Test
    void deleteSong_SongId_NoSuchId() {
        ResponseEntity<CartList> cart = restTemplate.getForEntity(URL, CartList.class);

        assertEquals(HttpStatus.OK, cart.getStatusCode());
        assertNotNull(cart.getBody());
        assertEquals(cartList.getCart().size(), cart.getBody().getCart().size());

        restTemplate.delete(URL + "/10000");

        cart = restTemplate.getForEntity(URL, CartList.class);

        assertEquals(HttpStatus.OK, cart.getStatusCode());
        assertNotNull(cart.getBody());
        assertEquals(cartList.getCart().size(), cart.getBody().getCart().size());
    }

    private boolean hasSongId(long id, CartList cartList) {
        for (Song s : cartList.getCart()) {
            if (s.getId().equals(id)) return true;
        }
        return false;
    }
}

