package com.playlister.cart.service;

import com.playlister.cart.model.CartList;
import com.playlister.cart.model.Song;
import com.playlister.cart.model.SongNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class CartService {

    CartList cartList;

    public CartService(CartList cartList) {
        this.cartList = cartList;
    }

    public CartList getCartList() {
        if (cartList.isEmpty()) return null;
        return cartList;
    }

    public void deleteSong(Long songId) {
        for (Song s : cartList.getCart()) {
            if (s.getId().equals(songId)) {
                cartList.getCart().remove(s);
                return;
            }
        }
        throw new SongNotFoundException();
    }

    public void setCartList(CartList cartList) {
        this.cartList = cartList;
    }
}
