package com.playlister.cart.controller;

import com.playlister.cart.model.CartList;
import com.playlister.cart.model.Song;
import com.playlister.cart.model.SongNotFoundException;
import com.playlister.cart.service.CartService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/cart")
public class CartController {
    CartService cartService;

    public CartController(CartService cartService) {
        this.cartService = cartService;
    }

    @GetMapping
    public ResponseEntity<CartList> getCartList() {
        CartList cart = cartService.getCartList();
        if (cart == null) return ResponseEntity.noContent().build();
        return ResponseEntity.ok(cart);
    }

    @PostMapping
    public CartList addCart() {
        if (cartService.getCartList() == null || cartService.getCartList().isEmpty()) {
            List<Song> list = new ArrayList<>();
            list.add(new Song(1L, "temp", "temp", "temp"));
            CartList cl = new CartList(list);
            cartService.setCartList(cl);
            return cl;
        }
        return cartService.getCartList();
    }

    @DeleteMapping("/{songId}")
    public ResponseEntity<?> deleteSong(@PathVariable Long songId) {
        try {
            cartService.deleteSong(songId);
        } catch (SongNotFoundException e) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.accepted().build();
    }
}
