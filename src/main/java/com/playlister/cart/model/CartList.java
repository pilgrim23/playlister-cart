package com.playlister.cart.model;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CartList {
    private List<Song> cart;

    public CartList(List<Song> cart) {
        this.cart = cart;
    }

    public CartList() {
        cart = new ArrayList<>();
    }

    public List<Song> getCart() {
        return cart;
    }

    public void setCart(List<Song> cart) {
        this.cart = cart;
    }

    public boolean isEmpty() {
        return cart.isEmpty();
    }
}
